#!/usr/bin/python
# -*- coding: UTF-8 -*-

import subprocess
import platform
import os
import sys
import json
import shutil
   
def get_pyexec():
    print(sys.executable)
    return sys.executable
    
def getCurrentRepo():
    BINPATH = os.path.abspath(os.path.dirname(__file__)).replace('\\', '/')
    setting_path = os.path.join(BINPATH, '.settings', 'repo-settings.json')
    if not os.path.exists(setting_path):
        return None
    json_str = open(setting_path, 'r', encoding='utf-8').read()
    if len(json_str) == 0:
        print("repo-settings.json corruption")
        exit(-1)
    settings = json.loads(open(setting_path, 'r', encoding='utf-8').read())
    if len(settings['cur_repo']) == 0:
        return None
    return settings['cur_repo'].replace('\\', '/')
    
def do_command(type):
    cur_repo = getCurrentRepo()
    if cur_repo is None:
        print("No repo")
        exit(-1)
    REPO_ROOT = cur_repo.replace("\\", "/")
    PYEXEC = get_pyexec()
    #cmd = PYEXEC +" "+REPO_ROOT+"/repo.py -t "+type+" "
    arg_list = [PYEXEC, REPO_ROOT+"/repo.py", "-t", type]
    arg_list.extend(sys.argv[1:])
    cmd = subprocess.Popen(arg_list, encoding='utf-8')
    ret = cmd.communicate()
    if cmd.returncode != 0:
        print(type, " failed")
        print("err message:", [ line.decode("utf-8") for line in ret if line is not None])
        return
    #os.system(cmd + ' '.join(sys.argv[1:]))
    
    
def switchRepo(repo_path):
    BINPATH = os.path.abspath(os.path.dirname(__file__)).replace('\\', '/')
    setting_path = os.path.join(BINPATH, '.settings', 'repo-settings.json')
    if not os.path.exists(setting_path):
        return
    json_str = open(setting_path, 'r', encoding='utf-8').read()
    if len(json_str) == 0:
        print("repo-settings.json corruption")
        exit(-1)
    settings = json.loads(json_str)
    settings['cur_repo'] = repo_path
    open(setting_path, 'w', encoding='utf-8').write(json.dumps(settings))