#!/bin/bash

abs_path=`pwd`
version=$(python -V 2>&1 | grep -Po '(?<=Python )(.+)')
if [[ -z "$version" ]]
then
    echo "Python not found"
	exit 1
fi
parsedVersion=$(echo "${version//./}")
if [[ "$parsedVersion" -lt "300" ]]
then 
    echo "Valid version"
	exit 1
fi

# generate your own plato file

cat > plato <<EOF
#!/bin/bash

if [ -x "\$(command -v python3)" ]; then
python3 ${abs_path}/plato.py \$*
else
python  ${abs_path}/plato.py \$*
fi
EOF

cat > platocpp <<EOF
#!/bin/bash

if [ -x "\$(command -v python3)" ]; then
python3 ${abs_path}/platocpp.py \$*
else
python  ${abs_path}/platocpp.py \$*
fi
EOF

cat > platolua <<EOF
#!/bin/bash

if [ -x "\$(command -v python3)" ]; then
python3 ${abs_path}/platolua.py \$*
else
python  ${abs_path}/platolua.py \$*
fi
EOF

cat > platogo <<EOF
#!/bin/bash

GOP=\`go env GOPATH\`
export PATH="\${GOP}/bin:\$PATH"

if [ -x "\$(command -v python3)" ]; then
python3 ${abs_path}/platogo.py \$*
else
python  ${abs_path}/platogo.py \$*
fi
EOF

cat > platopy <<EOF
#!/bin/bash

if [ -x "\$(command -v python3)" ]; then
python3 ${abs_path}/platopy.py \$*
else
python  ${abs_path}/platopy.py \$*
fi
EOF

chmod 755 ./plato
chmod 755 ./platocpp
chmod 755 ./platolua
chmod 755 ./platogo
chmod 755 ./platopy

ln -s ${abs_path}/plato /usr/bin/plato
ln -s ${abs_path}/platocpp /usr/bin/platocpp
ln -s ${abs_path}/platolua /usr/bin/platolua
ln -s ${abs_path}/platogo /usr/bin/platogo
ln -s ${abs_path}/platopy /usr/bin/platopy


