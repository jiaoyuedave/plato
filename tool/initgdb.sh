#!/bin/bash

OSNAME="Unknown"
ISN="Unknown"

function getLinuxDistribution() {
    #采用最简单和愚蠢的做法，不搞花活
    #最简单的判断，是否有os-release文件
    if type lsb_release > /dev/null 2>&1; then
        #是否有lsb-release文件
        OSNAME=$(lsb_release -si)
        ISN=apt-get
    elif [ -f /etc/os-release ]; then
	    . /etc/os-release
	    OSNAME=$NAME
	    ISN=apt-get
    elif [ -f /etc/debian_version ]; then
        #是否是debian
        OSNAME=Deepin
        ISN=apt-get
    elif [ -f /etc/rehat_version ]; then
        #是否是redhat
        OSNAME=RedHat
        ISN=yum
    else
        echo "unkown linux distribution!"
    fi
}

function root_need(){
    if [[ $EUID -ne 0 ]]; then
        echo "执行失败！当前用户非root！Error:This script must be run as root!" 1>&2
        exit 1
    fi
}

function initRedHat(){
    $ISN install python-devel -y
    $ISN install ncurses-devel -y
    $ISN install texinfo -y
    $ISN install svn -y
    $ISN remove gdb -y
}

function initDebian(){ 
    $ISN install svn -y
    $ISN install texinfo  -y 
    $ISN install libncurses5-dev -y
    $ISN remove gdb -y
}

function download_gdb(){
    wget https://ftp.gnu.org/gnu/gdb/gdb-10.1.tar.gz
    if [ $? -gt 0 ];then
        echo "下载失败请手动下载gdb 再执行脚本"
        exit -1
    fi
}

function install_gdb(){
    echo "开始解压"

    if [ ! -d ./gdb-10.1 ];then
        tar zxvf gdb-10.1.tar.gz
        if [ $? -gt -0 ];then
            echo "解压失败 请检查文件是否正确"
            exit 1
        fi 
    fi

    cd ./gdb-10.1

    if [ ! -d ./build ];then
        mkdir build 
    fi 

    cd ./build

    echo "开始配置 configure --prefix=/usr/local --with-python=/usr/bin/python --enable-tui=yes --with-system-gdbinit=/etc/gdb/gdbinit"
    ../configure --prefix=/usr/local --with-python=/usr/bin/python --enable-tui=yes --with-system-gdbinit=/etc/gdb/gdbinit

    if [ $? -gt 0 ];then
        echo "configure --prefix=/usr/local --with-python=/usr/bin/python --enable-tui=yes --with-system-gdbinit=/etc/gdb/gdbinit 执行失败，检查log文件"
        exit 1
    fi 

    make -j8
    if [ $? -gt 0 ];then
        echo "compile gdb error"
        exit 1
    fi
    make install

    if [ $? -gt 0 ];then
        echo "安装失败"
        exit 1
    fi 
}

function install_beauty_print(){
    svn co svn://gcc.gnu.org/svn/gcc/trunk/libstdc++-v3/python
    if [ ! -d /etc/gdb ];then
        mkdir /etc/gdb
    fi

    if [ -f /etc/gdb/gdbinit ];then
        rm /etc/gdb/gdbinit
    fi
    
    if [ ! -f /data/thirdparty/gdbinit ];then
        curl -S https://gitee.com/dennis-kk/plato/raw/master/tool/gdbinit
    fi
    
    mv gdbinit /etc/gdb/gdbinit
}


#检查目录
echo "请使用root用户运行此脚本"
root_need

getLinuxDistribution

if [ $OSNAME = "Unknown" ]; then
    echo "unkwon linux version!!"
    exit 1
fi

#刷新一下环境
if [ -f /etc/profile.d/gcc.sh ];then
    source /etc/profile.d/gcc.sh
fi

init$OSNAME

echo "开始下载gdb10.1 请保证data 目录下有有分配足够空间"
if [ ! -d /data ];then
    echo "data 目录不存在"
    mkdir /data
    chmod 777 data
fi

if [ ! -d /data/thirdparty ];then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
fi

# download gdb from thirparty
cd /data/thirdparty 

if [ ! -f gdb-10.1.tar.gz ];then
    download_gdb
fi

#安装beauty_print
install_beauty_print

#编译安装
install_gdb

#建立软链
if [ -f /usr/bin/gdb ];then
    rm /usr/bin/gdb
fi

ln -s /usr/local/bin/gdb /usr/bin/gdb



#清理
echo "正在清理"
cd /data/thirdparty

rm -r gdb-10.1
rm gdb-10.1.tar.gz
