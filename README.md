# Plato

Plato是对[rpc-repo](https://gitee.com/dennis-kk/rpc-repo/wikis/)的命令行封装, 让开发者便于使用, 将用户自己编写的服务代码与框架代码分离, 分离框架与用户代码仓库.

``` 
     ___  __    _   _____  ___
    / _ \/ /   /_\ /__   \/___\
   / /_)/ /   //_\\  / /\//  //
  / ___/ /___/  _  \/ / / \_//
  \/   \____/\_/ \_/\/  \___/

```

## 安装

### Windows

使用管理员权限运行:
```
install.bat
```

### Linux
运行:
```
install.sh
```
### Docker

```
curl -S https://gitee.com/dennis-kk/plato-docker/raw/master/install.sh | bash
```

## 初始化rpc-repo框架

```
plato pull v0.3.0-alpha
```
`v0.3.0-alpha`为`rpc-repo`的版本号.

## 切换rpc-repo框架版本

```
plato switch v0.3.0-alpha
```
`v0.3.0-alpha`为`rpc-repo`的版本号.

# rpc-repo语言框架命令

安装成功后可以使用如下命令操作不同的语言仓库, 不需要使用`-t`指定语言类型.

- platocpp
- platolua
- platogo
- platopy

# 运行时及SDK

| 语言  | 状态  | Client SDK  | Runtime  | 当前版本 |
|---|---|---|---|---|
| C++  | Stable Alpha | √ | √   | v0.3.0-alpha |
| Lua | Stable Alpha | √ | √ | v0.3.0-alpha |
| Python | Stable Alpha | √ | √ | v0.3.0-alpha |
| Go | Stable Alpha | ❌ | √ | v0.3.0-alpha |
| C# | 开发中 |2022.3|2022.3|
| TypeScript/JavaScript | 计划中 |2022.6 | 2022.6|
|Java|计划中  |2022.6|2022.6|
|Rust|计划中|2022.12| 2022.12|

# 特性列表
| 语言    | 提供运行时 | 可使用外部运行时 |需要宿主 | RPC代理模式 | HTTP代理模式 | 发布订阅协议 | 调用链 | 协程 | MOCK自动生成 | 接口测试生成 |
| ---    | ---       |  ---           |---     | ---        | ---         |  ---       | ---   | --- | ---        | ---       |
| C++    |  ✓       |     ❌        |   ❌    |  ✓         | ✓          | ✓         | ✓     | ✓   |    ❌       |    ✓     |
| Python |  ✓       |   ✓           |  ❌     |  ✓         | ❌         | ✓          | ✓     | ✓  |  ❌         |    ✓     |
| Go     |  ✓       |   ✓           |   ❌    |  ❌        | ❌         | ❌          | ❌    | ✓   | ✓         |      ❌   |
| Lua    |  ✓       |   ❌          |   ❌    | ❌         | ❌         | ❌          | ✓     | ✓   |  ❌       |     ❌    |