#!/usr/bin/python
# -*- coding: UTF-8 -*-

import util
import json
import os
import sys
import subprocess
import shutil

class Plato:
    def __init__(self, argv):
        self.__checkEnv()
        self.__installCommand()
        self.cur_repo = util.getCurrentRepo()
        self.settings = {}
        self.BINPATH = os.path.abspath(os.path.dirname(__file__)).replace('\\', '/')
        self.__parseSettings()
        if self.settings is not None:
            if 'cur_repo' in self.settings:
                self.cur_repo = self.settings['cur_repo']
        self.__doCommand(argv)
    
    def __doCommand(self, argv):
        command = argv[0]
        if command not in self.cmd_table:
            print('Command '+command + " not found")
            self.__help(None)
            exit(-1)
        self.cmd_table[command](argv[1:])
        
    def __parseSettings(self):
        setting_path = os.path.join(self.BINPATH, '.settings', 'repo-settings.json')
        if not os.path.exists(setting_path):
            return
        json_str = open(setting_path, 'r', encoding='utf-8').read()
        if len(json_str) == 0:
            os.remove(json_str)
            return
        self.settings = json.loads(open(setting_path, 'r', encoding='utf-8').read())        
           
    def __pull(self, argv):
        branch = ""
        if len(argv) != 0:
            branch = argv[0]
        else:
            print('Need a branch name')
            exit(-1)
        repo_root_path = os.path.join(self.BINPATH, 'repo', branch)
        if not os.path.exists(repo_root_path):
            os.makedirs(repo_root_path)
        else:
            print('Branch already pulled')
            exit(-1)
        os.chdir(repo_root_path)
        clone_cmd = 'git clone -b {0} {1} --depth=1'
        repo_url = 'https://gitee.com/dennis-kk/rpc-repo.git'
        if os.system(clone_cmd.format(branch, repo_url)) != 0:
            print("Pull repo failed " + repo_url + " " + branch)
            exit(-1)
        repo_path = os.path.join(repo_root_path, 'rpc-repo')
        self.settings['cur_repo'] = repo_path
        if 'repo_settings' not in self.settings:
            self.settings['repo_settings'] = []
        self.settings['repo_settings'].append({'repo_root':repo_path, 'version':branch})
        setting_path = os.path.join(self.BINPATH, '.settings', 'repo-settings.json')
        if not os.path.exists(os.path.join(self.BINPATH, '.settings')):
            os.makedirs(os.path.join(self.BINPATH, '.settings'))
        open(setting_path, 'w', encoding='utf-8').write(json.dumps(self.settings))
        self.cur_repo = repo_path
        
    def __remove(self, argv):
        branch = ""
        if len(argv) != 0:
            branch = argv[0]
        else:
            print('Need a branch name')
            exit(-1)
        repo_root_path = os.path.join(self.BINPATH, 'repo', branch)
        if not os.path.exists(repo_root_path):
            print("Branch not found")
            exit(-1)
        repo_path = os.path.join(repo_root_path, 'rpc-repo')
        try:
            shutil.rmtree(repo_path)
        except:
            print('remove '+branch+' failed')
            exit(-1)
        index = 0
        for setting in self.settings["repo_settings"]:
            if setting['repo_root'] == repo_path:
                del self.settings["repo_settings"][index]
                break
            index += 1
        if self.settings['cur_repo'] == repo_path:
            if len(self.settings["repo_settings"]) > 0:
                self.settings['cur_repo'] = self.settings["repo_settings"][0]
            else:
                self.settings['cur_repo'] = ""           
        setting_path = os.path.join(self.BINPATH, '.settings', 'repo-settings.json')
        open(setting_path, 'w', encoding='utf-8').write(json.dumps(self.settings))
        self.cur_repo = repo_path
        
    def __switch(self, argv):
        if len(argv) == 0:
            print('Need a branch')
            exit(-1)
        util.switchRepo(os.path.join(self.BINPATH, 'repo', argv[0], 'rpc-repo'))
        
    def __current_repo(self, argv):
        cur_repo = util.getCurrentRepo()
        if cur_repo is None:
            print("No repo")
            exit(-1)
        split_str = cur_repo.split('/')
        branch = split_str[len(split_str)-2]
        print(branch)
        
    def __help(self, argv):           
        print('     ___  __    _   _____  ___ ')
        print('    / _ \\/ /   /_\\ /__   \\/___\\')
        print('   / /_)/ /   //_\\\  / /\\//  //')
        print('  / ___/ /___/  _  \\/ / / \\_// ')
        print('  \/   \\____/\\_/ \\_/\\/  \\___/  ')
        print('')
        print("  Plato " + open(os.path.join(self.BINPATH, "VERSION")).read())
        print("")
        print("      plato pull [branch]   Pull repository as given branch")
        print("      plato switch [branch] Switch repository as given branch")
        print("      plato remove [branch] Remove branch")
        print("      plato current         Show current branch")
        print("      plato upgrade         Upgrade manage tool of repository")
        print("      plato repo            Show all repo")
        print("      plato help            Help Doc.")
       
    def __installCommand(self):
        self.cmd_table = {}
        self.cmd_table['pull'] = self.__pull
        self.cmd_table['switch'] = self.__switch
        self.cmd_table['remove'] = self.__remove
        self.cmd_table['current'] = self.__current_repo
        self.cmd_table['repo'] = self.__showRepo
        self.cmd_table['help'] = self.__help
        self.cmd_table['upgrade'] = self.__upgrade
        
    def __upgrade(self, argv):
        os.system("python "+os.path.join(self.cur_repo, "repo.py") + " --upgrade")
        
    def __showRepo(self, argv):
        _dirs = []
        list = os.listdir(os.path.join(self.BINPATH, "repo"))
        for i in range(0, len(list)):
            print(list[i])
        if len(list) == 0:
            print('No repo')
        
    def __checkEnv(self):
        self.__check_git_env()
        self.__check_cmake_evn()
        
    def __check_git_env(self):
        error = os.system("git --version")
        if error != 0:
            print("git not found")
            exit(-1)
        return True
        
    def __check_cmake_evn(self):
        error = os.system("cmake --version")
        if error != 0:
            print("cmake not found")
            exit(-1)
        else:
            out_bytes = subprocess.check_output(['cmake', '--version'])
            out_text = out_bytes.decode('utf-8')
            strs = out_text.split(" ")
            if int(strs[2].split(".")[0]) < 3:
                print("Need cmake version >= 3")
                exit(-1)
            else:
                if int(strs[2].split(".")[1]) < 5:
                    print("Need cmake version >= 3.5")
                    exit(-1)
            
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("type 'plato help' for more information")
        exit(-1)
    Plato(sys.argv[1:])
        