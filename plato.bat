@echo off
python --version >nul 2>nul
if %errorlevel%==0 (python %~dp0/plato.py %*) else (echo Python not found)